package com.ssh.speedfreak;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class LabyrinthView extends SurfaceView implements SurfaceHolder.Callback, SensorEventListener {

    static Random random = new Random();

    private static float BALL_SCALE = 0.8f;

    private static float speed = 1;
    private static float ACCEL_WEIGHT = 3f;

    private static float reverseRunRatio = 0.2f;
    private static float parallelRunRatio = 0.1f;
    private static float sineCurveUnstabilityRatio = 0.1f;
    private static float randomWalkUnstabilityRatio = 0.1f;

    private static final Paint TEXT_PAINT = new Paint();

    static {
        TEXT_PAINT.setColor(Color.WHITE);
        TEXT_PAINT.setTextSize(40f);
    }

    private Bitmap ballBitmap;  // ボール⇒今は自車の画像

    private Ball ball;
    private Map map;
    private ArrayList<BaseObject>[][] bikeList; // 自転車のリスト
    private ArrayList<BaseObject>[][] carList;  // 自動車のリスト
    private int[][] properBikeNum;             // 描画したい自転車の台数
    private int[][] properCarNum;              // 描画したい自動車の台数

    private int stageNo; // 現在のステージ番号

    private ImageView accel;
    private boolean accel_hold; //アクセルおしっぱの判定
    private ImageView brake;
    private boolean brake_hold; //ブレーキ おしっぱの判定
    private ImageView arrow_left;
    private boolean al_hold; //左ボタンおしっぱの判定
    private ImageView arrow_right;
    private boolean ar_hold; //右ボタンおしっぱの判定

    private float theta = 0; //角度

    public void setStage(int stage) { this.stageNo = stage; }

    interface Callback {
        public void onGoal();
        public void onBikeAccident();
        public void onCarAccident();
    }

    private Callback callback;

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public LabyrinthView(Context context) {
        super(context);

        getHolder().addCallback(this);

        // ボールのBitmapをロード
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = false;
        ballBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.redcar, opt);

        speed = 0;
        accel = (ImageView) ((com.ssh.speedfreak.MainActivity) context).findViewById(R.id.accel);
        brake = (ImageView) ((com.ssh.speedfreak.MainActivity) context).findViewById(R.id.brake);
        arrow_left = (ImageView) ((com.ssh.speedfreak.MainActivity) context).findViewById(R.id.arrow_left);
        arrow_right = (ImageView) ((com.ssh.speedfreak.MainActivity) context).findViewById(R.id.arrow_right);
        setTouch();
    }


    private DrawThread drawThread;

    private class DrawThread extends Thread {
        private boolean isFinished;

        @Override
        public void run() {
            while (!isFinished) {
                Canvas canvas = getHolder().lockCanvas();
                if (canvas != null) {
                    carMove();
                    drawLabyrinth(canvas);
                    getHolder().unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void startDrawThread() {
        stopDrawThread();

        drawThread = new DrawThread();
        drawThread.start();
    }

    public boolean stopDrawThread() {
        if (drawThread == null) {
            return false;
        }

        drawThread.isFinished = true;
        drawThread = null;

        return true;
    }

    public void drawLabyrinth(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        if (map == null) {
            map = new Map(this.getContext(), canvas.getWidth(), canvas.getHeight(), callback, stageNo, getResources());
            ACCEL_WEIGHT = (float)map.getMySpeed();
            BALL_SCALE = (float)map.getMyBallScale();
            reverseRunRatio = (float)map.getReverseRunRatio();
            parallelRunRatio = (float)map.getParallelRunRatio();
            sineCurveUnstabilityRatio = (float)map.getSineCurveUnstabilityRatio();
            randomWalkUnstabilityRatio = (float)map.getRandomWalkUnstabilityRatio();
            allocateVehicleList();
        }

        if (ball == null) {
            ball = new Ball(ballBitmap, map.getStartBlock(), map.getBlockSize(), BALL_SCALE);
            ball.setOnMoveListener(map);
        }

        if (map != null) {
            int curScreenX = map.getCurScreenX();
            int curScreenY = map.getCurScreenY();
            if (bikeList != null) {
                if (bikeList[curScreenY][curScreenX].size() == 0) {
                    decideProperBikeNum();
                    initializeBikeList(properBikeNum[curScreenY][curScreenX]);
                } else if (bikeList[curScreenY][curScreenX].size() <= properBikeNum[curScreenY][curScreenX]) {
                    if (random.nextInt(30) == 0)
                        addBike(1);
                }
            }
            if (carList != null) {
                if (carList[curScreenY][curScreenX].size() == 0) {
                    decideProperCarNum();
                    initializeCarList(properCarNum[curScreenY][curScreenX]);
                } else if (carList[curScreenY][curScreenX].size() <= properCarNum[curScreenY][curScreenX]) {
                    if (random.nextInt(30) == 0)
                        addCar(1);
                }
            }
        }

        map.setBall(ball);
        map.drawMap(canvas);

        ball.draw(canvas);

        if (map != null && bikeList != null) {
            drawBikeList(canvas);
            //canvas.drawText("#properBikeNum = " + properBikeNum[map.getCurScreenY()][map.getCurScreenX()], 10, 50, TEXT_PAINT);
            //canvas.drawText("#bike = " + bikeList[map.getCurScreenY()][map.getCurScreenX()].size(), 10, 100, TEXT_PAINT);
        }
        if (map != null && carList != null) {
            drawCarList(canvas);
        }

        /*
        if (sensorValues != null) {
            canvas.drawText("sensor[0] = " + sensorValues[0], 10, 150, TEXT_PAINT);
            canvas.drawText("sensor[1] = " + sensorValues[1], 10, 200, TEXT_PAINT);
            canvas.drawText("sensor[2] = " + sensorValues[2], 10, 250, TEXT_PAINT);
        }*/
        // canvasサイズを取得する
        /*if (canvas != null) {
            canvas.drawText("width: " + canvas.getWidth(), 10, 300, TEXT_PAINT);
            canvas.drawText("height: " + canvas.getHeight(), 10, 350, TEXT_PAINT);
        }*/

    }

    public void startSensor() {
        sensorValues = null;

        SensorManager sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    public void stopSensor() {
        SensorManager sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startDrawThread();

        startSensor();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopDrawThread();

        stopSensor();
    }

    private static final float ALPHA = 0.8f;
    private float[] sensorValues;

    //車が動くメソッド
    @Override
    public void onSensorChanged(SensorEvent event) {
//        if (sensorValues == null) {
//            sensorValues = new float[3];
//            sensorValues[0] = event.values[0];
//            sensorValues[1] = event.values[1];
//            sensorValues[2] = event.values[2];
//            return;
//        }
//
//        sensorValues[0] = sensorValues[0] * ALPHA + event.values[0] * (1f - ALPHA);
//        sensorValues[1] = sensorValues[1] * ALPHA + event.values[1] * (1f - ALPHA);
//        sensorValues[2] = sensorValues[2] * ALPHA + event.values[2] * (1f - ALPHA);
//
//        if (ball != null) {
//            ball.move(-sensorValues[0] * ACCEL_WEIGHT, sensorValues[1] * ACCEL_WEIGHT);
//        }
//
//        if (map != null) {
//            int curScreenX = map.getCurScreenX();
//            int curScreenY = map.getCurScreenY();
//            if (bikeList != null && bikeList[curScreenY][curScreenX] != null) moveBikeList();
//            if (carList != null && carList[curScreenY][curScreenX] != null) moveCarList();
//        }
        // originally,
        // moveBikeList();
        // moveCarList();

    }

    //ボタン押してる間のみ移動するようにする
    private void carMove(){
        float move_x;
        float move_y;

        if( ball != null) {
            if( accel_hold == true){
                turn();
                speed = speed * (float)1.05;
                if(speed > 10) speed = 10;
                move_x = (float) (speed * Math.cos(Math.toRadians(theta)));
                move_y = (float) (speed * Math.sin(Math.toRadians(theta)));
                ball.move(move_x * ACCEL_WEIGHT, move_y * ACCEL_WEIGHT);
            }else if(speed > 1){
                turn();
                speed = speed * (float)0.995;
                move_x = (float) (speed * Math.cos(Math.toRadians(theta)));
                move_y = (float) (speed * Math.sin(Math.toRadians(theta)));
                ball.move(move_x * ACCEL_WEIGHT, move_y * ACCEL_WEIGHT);
            }else speed = 1;

            if( brake_hold == true && speed > 1){
                turn();
                speed = speed * (float)0.85;
                move_x = (float) (speed * Math.cos(Math.toRadians(theta)));
                move_y = (float) (speed * Math.sin(Math.toRadians(theta)));
                ball.move(move_x * ACCEL_WEIGHT, move_y * ACCEL_WEIGHT);
            }
        }
        if (map != null) {
            int curScreenX = map.getCurScreenX();
            int curScreenY = map.getCurScreenY();
            if (bikeList != null && bikeList[curScreenY][curScreenX] != null) moveBikeList();
            if (carList != null && carList[curScreenY][curScreenX] != null) moveCarList();
        }
    }

    //左右操作
    private void turn(){
        if(al_hold == true){
            theta -= 5;
        }else if(ar_hold == true){
            theta += 5;
        }
        if(theta == 360) theta = 0;
        if(theta == -360) theta = 0;
    }

    //ボタンのタッチイベント
    private void setTouch(){
        accel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        accel_hold = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        accel_hold = false;
                        break;
                }
                return true;
            }
        });
       brake.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        brake_hold = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        brake_hold = false;
                        break;
                }
                return true;
            }
        });
        arrow_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        al_hold = true;
                        break;
                    case MotionEvent.ACTION_UP:
                       al_hold = false;
                        break;
                }
                return true;
            }
        });
        arrow_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        ar_hold = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        ar_hold = false;
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }



    public void allocateVehicleList() {
        int screenNumX = map.getScreenNumX();
        int screenNumY = map.getScreenNumY();
        bikeList = new ArrayList[screenNumY][screenNumX];
        for (int scY = 0; scY < screenNumY; scY++) {
            for (int scX = 0; scX < screenNumX; scX++) {
                bikeList[scY][scX] = new ArrayList<BaseObject>();
            }
        }
        properBikeNum = new int[screenNumY][screenNumX];

        carList = new ArrayList[screenNumY][screenNumX];
        for (int scY = 0; scY < screenNumY; scY++) {
            for (int scX = 0; scX < screenNumX; scX++) {
                carList[scY][scX] = new ArrayList<BaseObject>();
            }
        }
        properCarNum = new int[screenNumY][screenNumX];
    }

    public void decideProperBikeNum() {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int countNormalRoad = 0;
        for (int y = 0; y < blockNumY; y++) {
            for (int x = 0; x < blockNumX; x++) {
                Map.Block block = map.getBlock(y, x);
                if (block.ifNormalRoad())
                    countNormalRoad++;
            }
        }
        properBikeNum[curScreenY][curScreenX] = (int)((float)countNormalRoad * map.getBikeDensity());
    }

    // 自転車を初期配置
    public void initializeBikeList(int num) {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        ArrayList<Map.Block> startList = new ArrayList<Map.Block>();
        for (int y = 0; y < blockNumY; y++) {
            for (int x = 0; x < blockNumX; x++) {
                Map.Block block = map.getBlock(y, x);
                if (block.ifNormalRoad())
                    startList.add(block);
            }
        }
        for (int bikeNo = 0; bikeNo < num && startList.size() > 0; bikeNo++) {
            Map.Block start = startList.get(random.nextInt(startList.size()));
            boolean reverse  = (Math.random() <= reverseRunRatio); // 逆走するかどうか
            boolean parallel = (Math.random() <= parallelRunRatio); // 並走するかどうか
            int direction = (reverse) ? start.selectInDirection() : start.selectOutDirection();
            if (direction != Map.Block.DIR_NONE) {
                float speed = (float) map.getBikeSpeed();
                float radius = (float) map.getBikeRadius();
                float bikeMargin = (float) map.getBikeMargin();
                if (parallel) bikeMargin *= 1.7f;
                if (reverse) bikeMargin = 1.0f - bikeMargin;
                Vehicle bike = new Vehicle(map, start, direction, speed, radius, bikeMargin, reverse, parallel);
                if (Math.random() <= sineCurveUnstabilityRatio)
                    bike.setStability(Vehicle.Stability.SINE_CURVE, 1.0f);
                else if (Math.random() <= randomWalkUnstabilityRatio)
                    bike.setStability(Vehicle.Stability.RANDOM_WALK, 1.0f);
                bikeList[curScreenY][curScreenX].add(bike);
            }
            else bikeNo--;
            startList.remove(start);
        }
    }

    // 自転車を追加
    public void addBike(int num) {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();

        ArrayList<Map.Block> startList = map.getObstacleStart();
        if (startList.size() == 0) return;
        for (int bikeNo = 0; bikeNo < num; bikeNo++) {
            boolean reverse  = (Math.random() <= reverseRunRatio); // 逆走するかどうか
            boolean parallel = (Math.random() <= parallelRunRatio); // 並走するかどうか
            Map.Block start = startList.get(random.nextInt(startList.size()));
            int direction = (reverse) ? start.selectInDirection() : start.selectOutDirection();
            if (direction == Map.Block.DIR_NONE) { bikeNo--; continue; }
            float speed = (float) map.getBikeSpeed();
            float radius = (float) map.getBikeRadius();
            float bikeMargin = (float) map.getBikeMargin();
            if (parallel) bikeMargin *= 1.7f;
            if (reverse) bikeMargin = 1.0f - bikeMargin;
            Vehicle bike = new Vehicle(map, start, direction, speed, radius, bikeMargin, reverse, parallel);
            if (Math.random() <= sineCurveUnstabilityRatio)
                bike.setStability(Vehicle.Stability.SINE_CURVE, 1.0f);
            else if (Math.random() <= randomWalkUnstabilityRatio)
                bike.setStability(Vehicle.Stability.RANDOM_WALK, 1.0f);
            bikeList[curScreenY][curScreenX].add(bike);
        }
    }

    public void drawBikeList(Canvas canvas) {
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int blockSize = map.getBlockSize();
        int screenWidth  = blockSize * blockNumX;
        int screenHeight = blockSize * blockNumY;
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        ArrayList<BaseObject> bikes = bikeList[curScreenY][curScreenX];
        for (int i = 0; i < bikes.size(); i++) {
            BaseObject bike = bikes.get(i);
            if (bike.isAvailable(screenWidth, screenHeight)) {
                bike.draw(canvas);
            }
        }
    }

    public void moveBikeList() {
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int blockSize = map.getBlockSize();
        int screenWidth  = blockSize * blockNumX;
        int screenHeight = blockSize * blockNumY;
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        ArrayList<BaseObject> bikes = bikeList[curScreenY][curScreenX];
        for (int i = 0; i < bikes.size(); i++) {
            BaseObject bike = bikes.get(i);
            if (bike.isAvailable(screenWidth, screenHeight)) {
                bikes.get(i).move(map);
            }
            if (bike.isAvailable(screenWidth, screenHeight)) {
                if (bike.calcDistance(bike, ball.getCenter()) < (ball.getBallScale()*blockSize*0.5 + map.getBikeRadius())) {
                    callback.onBikeAccident(); // 衝突の検知
                }
            }
            else {
                bikes.remove(bike);
                i--;
            }
        }
    }


    public void decideProperCarNum() {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int countNormalRoad = 0;
        for (int y = 0; y < blockNumY; y++) {
            for (int x = 0; x < blockNumX; x++) {
                Map.Block block = map.getBlock(y, x);
                if (block.ifNormalRoad())
                    countNormalRoad++;
            }
        }
        properCarNum[curScreenY][curScreenX] = (int)((float)countNormalRoad * map.getCarDensity());
    }

    // 自動車を初期配置
    public void initializeCarList(int num) {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        ArrayList<Map.Block> startList = new ArrayList<Map.Block>();
        for (int y = 0; y < blockNumY; y++) {
            for (int x = 0; x < blockNumX; x++) {
                Map.Block block = map.getBlock(y, x);
                if (block.ifNormalRoad())
                    startList.add(block);
            }
        }
        for (int carNo = 0; carNo < num && startList.size() > 0; carNo++) {
            Map.Block start = startList.get(random.nextInt(startList.size()));
            int direction = start.selectOutDirection();
            if (direction != Map.Block.DIR_NONE) {
                float speed = (float) map.getCarSpeed();
                float radius = (float) map.getCarRadius();
                float carMargin = (float) map.getCarMargin();
                Vehicle car = new Vehicle(map, start, direction, speed, radius, carMargin, false, false);
                carList[curScreenY][curScreenX].add(car);
            }
            else carNo--;
            startList.remove(start);
        }
    }

    // 自動車を追加
    public void addCar(int num) {
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();

        ArrayList<Map.Block> startList = map.getObstacleStart();
        if (startList.size() == 0) return;
        for (int carNo = 0; carNo < num; carNo++) {
            Map.Block start = startList.get(random.nextInt(startList.size()));
            int direction = start.selectOutDirection();
            if (direction == Map.Block.DIR_NONE) { carNo--; continue; }
            float speed = (float) map.getCarSpeed();
            float radius = (float) map.getCarRadius();
            float bikeMargin = (float) map.getCarMargin();
            Vehicle car = new Vehicle(map, start, direction, speed, radius, bikeMargin, false, false);
            carList[curScreenY][curScreenX].add(car);
        }
    }

    public void drawCarList(Canvas canvas) {
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int blockSize = map.getBlockSize();
        int screenWidth  = blockSize * blockNumX;
        int screenHeight = blockSize * blockNumY;
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        ArrayList<BaseObject> cars = carList[curScreenY][curScreenX];
        for (int i = 0; i < cars.size(); i++) {
            BaseObject car = cars.get(i);
            if (car.isAvailable(screenWidth, screenHeight)) {
                car.draw(canvas);
            }
        }
    }

    public void moveCarList() {
        int blockNumX = map.getBlockNumX();
        int blockNumY = map.getBlockNumY();
        int blockSize = map.getBlockSize();
        int screenWidth  = blockSize * blockNumX;
        int screenHeight = blockSize * blockNumY;
        int curScreenX = map.getCurScreenX();
        int curScreenY = map.getCurScreenY();
        ArrayList<BaseObject> cars = carList[curScreenY][curScreenX];
        for (int i = 0; i < cars.size(); i++) {
            BaseObject car = cars.get(i);
            if (car.isAvailable(screenWidth, screenHeight)) {
                cars.get(i).move(map);
            }
            if (car.isAvailable(screenWidth, screenHeight)) {
                if (car.calcDistance(car, ball.getCenter()) < (ball.getBallScale()*blockSize*0.5 + map.getCarRadius())) {
                    callback.onCarAccident(); // 衝突の検知
                }
            }
            else {
                cars.remove(car);
                i--;
            }
        }
    }

}
