package com.ssh.speedfreak;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Point;
import android.graphics.Matrix;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class Ball {

    private static final Paint PAINT = new Paint();

    private Bitmap ballBitmap;
    private final float blockRate; // ブロックの大きさに調整するために必要な拡大縮小レート（blockSize/画像の解像度）
    private final float ballScale; // ボールだけに適用する拡大縮小レート（表示解像度/blockSize）

    private final Rect rect;
    private final Rect srcRect;
    private static final int historyLength = 10;
    private final Point[] history = new Point[historyLength];

    private OnMoveListener listener;

    public void setOnMoveListener(OnMoveListener l) {
        listener = l;
    }

    float min(float x, float y) { return (x<=y) ? x : y; }

    public Ball(Bitmap bmp, Map.Block startBlock, int blockSize, float scale) {
        this(bmp, startBlock.rect.left, startBlock.rect.top, blockSize, scale);
    }

    public Ball(Bitmap bmp, int left, int top, int blockSize, float scale) {
        ballBitmap = bmp;
        ballScale = scale;

        blockRate = min( (float)blockSize / bmp.getWidth(), (float)blockSize / bmp.getHeight() );
        int right = left + Math.round(bmp.getWidth() * blockRate * scale);
        int bottom = top + Math.round(bmp.getHeight() * blockRate * scale);
        rect = new Rect(left, top, right, bottom);

        srcRect = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());

        for (int i = 0; i < historyLength; i++) {
            history[i] = new Point(rect.left, rect.top);
        }
    }

    void draw(Canvas canvas) {
        //canvas.drawBitmap(ballBitmap, srcRect, rect, PAINT);

        int dx = history[0].x - history[historyLength-1].x;
        int dy = history[0].y - history[historyLength-1].y;
        float theta = 0f;
        if (!(dx == 0 && dy == 0))
            theta = (float)Math.atan2(dy, dx);

        theta += 2.0*Math.PI;
        theta *= 180.0 / Math.PI;
        theta += 90.0;  // 横に向けるため無条件に90度回転
        Matrix mat = new Matrix();
        mat.postScale(blockRate*ballScale, blockRate*ballScale);
        mat.postRotate(theta);
        Bitmap ballBitmap2 = Bitmap.createBitmap(ballBitmap, 0, 0, ballBitmap.getWidth(), ballBitmap.getHeight(), mat, true);
        //canvas.drawBitmap(ballBitmap2, srcRect, rect, PAINT);
        double L = ballBitmap.getHeight();
        while (theta > 90.0) theta -= 90.0;
        double dleft = L*Math.sin(theta*Math.PI/180.0) + Math.cos((theta+45)*Math.PI/180.0) * L/Math.sqrt(2) - L/2.0;
        double dtop  =                                   Math.sin((theta+45)*Math.PI/180.0) * L/Math.sqrt(2) - L/2.0;
        canvas.drawBitmap(ballBitmap2, rect.left-(int)dleft, rect.top-(int)dtop, PAINT);


        /*
        th++;
        Matrix mat = new Matrix();
        //mat.preTranslate(-50,-50);
        //mat.postScale(0.7f,0.7f);
        mat.postRotate(th, 50, 50);
        //mat.postTranslate(50,50);
        Bitmap ballBitmap2 = Bitmap.createBitmap(ballBitmap, 0, 0, ballBitmap.getWidth(), ballBitmap.getHeight(), mat, true);
        canvas.drawBitmap(ballBitmap2, rect.left, rect.top, PAINT);
         */

    }

    public float getBallScale() { return ballScale; }
    public Point getCenter() {
        Point p = new Point( (rect.left + rect.right) / 2, (rect.top + rect.bottom) / 2 );
        return p;
    }

    void forceMoveLeftMost(int x) {
        rect.left = x;
        rect.right = rect.left + Math.round(ballBitmap.getWidth() * blockRate * ballScale);
    }
    void forceMoveRightMost(int x) {
        rect.right = x;
        rect.left = rect.right - Math.round(ballBitmap.getWidth() * blockRate * ballScale);
    }
    void forceMoveTopMost(int y) {
        rect.top = y;
        rect.bottom = rect.top + Math.round(ballBitmap.getHeight() * blockRate * ballScale);
    }
    void forceMoveBottomMost(int y) {
        rect.bottom = y;
        rect.top = rect.bottom - Math.round(ballBitmap.getHeight() * blockRate * ballScale);
    }

    void move(float xOffset, float yOffset) {
        int lastX = rect.left;
        int lastY = rect.top;

        int align = yOffset >= 0 ? 1 : -1;
        while (!tryMoveVertical(yOffset)) {
            yOffset -= align;
        }

        align = xOffset >= 0 ? 1 : -1;
        while (!tryMoveHorizontal(xOffset)) {
            xOffset -= align;
        }

        if (rect.left != lastX || rect.top != lastY) {
            for (int i = historyLength-1; i > 0; i--) {
                history[i].x = history[i - 1].x;
                history[i].y = history[i - 1].y;
            }
            history[0].x = rect.left;
            history[0].y = rect.top;
        }

        listener.onSpecialBlock(rect.left, rect.top, rect.right, rect.bottom); // ゴール判定の反応が悪いのを改善
    }

    private boolean tryMoveHorizontal(float xOffset) {
        int left = rect.left + Math.round(xOffset);
        int right = left + rect.width();

        if (!listener.canMove(left, rect.top, right, rect.bottom)) {
            return false;
        }

        rect.left = left;
        rect.right = right;
        return true;
    }

    private boolean tryMoveVertical(float yOffset) {
        int top = rect.top + Math.round(yOffset);
        int bottom = top + rect.height();

        if (!listener.canMove(rect.left, top, rect.right, bottom)) {
            return false;
        }

        rect.top = top;
        rect.bottom = bottom;
        return true;
    }

    public interface OnMoveListener {
        public boolean canMove(int left, int top, int right, int bottom);
        public void    onSpecialBlock(int left, int top, int right, int bottom);
    }

}
