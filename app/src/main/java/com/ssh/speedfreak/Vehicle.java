package com.ssh.speedfreak;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.Random;


public class Vehicle extends BaseObject {

    static Random random = new Random();
    float min(float x, float y) { return (x <= y) ? x : y; }

    private final Paint paint = new Paint();

    private float radius; // 自転車の大きさ[pixel]

    private int blockX, blockY; // ブロック座標
    private int blockSize;      // ブロックの大きさ[pixel]

    private Rect within; // 現在所属のブロックにほぼ同じ
    private final float marginRate; // 路肩からの距離（blockSizeとの比; 0～1）
    private final float speed;       // 再描画あたりの移動距離[pixel]
    private final boolean reverse;  // 逆走するかどうか
    private final boolean parallel; // 並走するかどうか

    private int directionIN, directionOUT; // 現ブロックへ入るとき・出るときの移動方向

    private float dx, dy; // 直進時の再描画あたりの変化量
    private float cx, cy; // 右折・左折時の回転中心
    private float r, theta, dtheta; // 右折・左折時に使用

    public enum Stability {
        SINE_CURVE,
        RANDOM_WALK,
        STABLE,
    }

    private Stability stability = Stability.STABLE;
    private float unstableParam = 0.0f;     // フラツキの更新に必要なパラメータ
    private float unstableParamDiff = 0.0f; // それを再描画あたりにどのくらい変化させるか
    private float unstableAmount = 0.0f;  // フラツキ度（この値だけ正規の位置からずらす）
    private float stableXPosition, stableYPosition; // フラツキがなかった場合の位置

    public void setStability(Stability stability, float degree) {
        this.stability = stability;
        switch (stability) {
            case SINE_CURVE:
                unstableParam = 0.0f;
                unstableParamDiff = (float) (2.0 * Math.PI * speed / blockSize * degree);
                unstableAmount = 0.0f;
                break;
            case RANDOM_WALK:
                unstableParam = min(marginRate, 1.0f-marginRate) * blockSize * 0.95f;
                unstableAmount = 0.0f;
                break;
        }
    }



    // direction: 最初の進行方向（Map.Block.DIR_UP, DOWN, LEFT, RIGHT にて指定）
    // speed: 再描画あたりの移動距離[pixel]
    // radius: 描画の際の円の半径[pixel]
    // marginRate: 路肩からの距離（blockSizeとの比; 0～1）
    // reverse: 逆走するかどうか
    // parallel: 並走するかどうか
    Vehicle(Map map, Map.Block startBlock, int direction, float speed, float radius, float marginRate, boolean reverse, boolean parallel) {
        paint.setColor(Color.rgb(random.nextInt(80)+30, random.nextInt(80)+30, random.nextInt(80)+30));
        directionIN = directionOUT = direction; // 最初は必ず直進（でいいはず）
        this.speed = speed;
        this.radius = radius;
        this.marginRate = marginRate;
        this.reverse = reverse;
        this.parallel = parallel;
        blockSize = map.getBlockSize();
        blockX = map.getBlockX(startBlock);
        blockY = map.getBlockY(startBlock);
        within = new Rect(blockX*blockSize, blockY*blockSize, (blockX+1)*blockSize, (blockY+1)*blockSize);
        setParams(true);
    }

    public void setParams(boolean randomize) {

        if (directionIN == directionOUT) { // 直進
            switch (directionIN) {
                case Map.Block.DIR_UP:
                    stableXPosition = within.left + (within.right - within.left) * marginRate;
                    stableYPosition = within.bottom;
                    if (randomize) stableYPosition -= Math.random() * blockSize;
                    dx = 0;
                    dy = -speed;
                    break;
                case Map.Block.DIR_DOWN:
                    stableXPosition = within.right + (within.left - within.right) * marginRate;
                    stableYPosition = within.top;
                    if (randomize) stableYPosition += Math.random() * blockSize;
                    dx = 0;
                    dy = speed;
                    break;
                case Map.Block.DIR_LEFT:
                    stableXPosition = within.right;
                    stableYPosition = within.bottom + (within.top - within.bottom) * marginRate;
                    if (randomize) stableXPosition -= Math.random() * blockSize;
                    dx = -speed;
                    dy = 0;
                    break;
                case Map.Block.DIR_RIGHT:
                    stableXPosition = within.left;
                    stableYPosition = within.top + (within.bottom - within.top) * marginRate;
                    if (randomize) stableXPosition += Math.random() * blockSize;
                    dx = speed;
                    dy = 0;
                    break;
            }
        }
        else { // 右折・左折
            // 左折４パターン
            if (directionIN == Map.Block.DIR_UP && directionOUT == Map.Block.DIR_LEFT) {
                cx = within.left; cy = within.bottom;
                r = blockSize * marginRate;
                theta = 0;  dtheta = -speed / r;
                if (randomize) theta -= (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_RIGHT && directionOUT == Map.Block.DIR_UP) {
                cx = within.left; cy = within.top;
                r = blockSize * marginRate;
                theta = (float)Math.PI * 0.5f;  dtheta = -speed / r;
                if (randomize) theta -= (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_DOWN && directionOUT == Map.Block.DIR_RIGHT) {
                cx = within.right; cy = within.top;
                r = blockSize * marginRate;
                theta = (float)Math.PI;  dtheta = -speed / r;
                if (randomize) theta -= (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_LEFT && directionOUT == Map.Block.DIR_DOWN) {
                cx = within.right; cy = within.bottom;
                r = blockSize * marginRate;
                theta = (float)Math.PI * 1.5f;  dtheta = -speed / r;
                if (randomize) theta -= (float)Math.PI / 2.0f * Math.random();
            }
            // 右折４パターン
            else if (directionIN == Map.Block.DIR_UP && directionOUT == Map.Block.DIR_RIGHT) {
                cx = within.right; cy = within.bottom;
                r = blockSize * (1.0f - marginRate);
                theta = (float)Math.PI;  dtheta = speed / r;
                if (randomize) theta += (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_LEFT && directionOUT == Map.Block.DIR_UP) {
                cx = within.right; cy = within.top;
                r = blockSize * (1.0f - marginRate);
                theta = (float)Math.PI * 0.5f;  dtheta = speed / r;
                if (randomize) theta += (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_DOWN && directionOUT == Map.Block.DIR_LEFT) {
                cx = within.left; cy = within.top;
                r = blockSize * (1.0f - marginRate);
                theta = 0.0f;  dtheta = speed / r;
                if (randomize) theta += (float)Math.PI / 2.0f * Math.random();
            }
            else if (directionIN == Map.Block.DIR_RIGHT && directionOUT == Map.Block.DIR_DOWN) {
                cx = within.left; cy = within.bottom;
                r = blockSize * (1.0f - marginRate);
                theta = (float)Math.PI * 1.5f;  dtheta = speed / r;
                if (randomize) theta += (float)Math.PI / 2.0f * Math.random();
            }
            stableXPosition = cx + r * (float)Math.cos(theta);
            stableYPosition = cy + r * (float)Math.sin(theta);
        }
        //xPosition = stableXPosition;
        //yPosition = stableYPosition;
        addStability();
    }

    @Override
    public void move(Map map) {
        if (directionIN == directionOUT) { // 直進
            stableXPosition += dx;
            stableYPosition += dy;
        }
        else { // 右折・左折
            theta += dtheta;
            stableXPosition = cx + r * (float)Math.cos(theta);
            stableYPosition = cy + r * (float)Math.sin(theta);
        }
        //xPosition = stableXPosition;
        //yPosition = stableYPosition;
        addStability();

        if ( within.contains((int)stableXPosition, (int)stableYPosition)) return;

        switch (directionOUT) {
            case Map.Block.DIR_UP:
                blockY--;
                break;
            case Map.Block.DIR_DOWN:
                blockY++;
                break;
            case Map.Block.DIR_LEFT:
                blockX--;
                break;
            case Map.Block.DIR_RIGHT:
                blockX++;
                break;
        }

        if ( blockX < 0 || blockX >= map.getBlockNumX() || blockY < 0 || blockY >= map.getBlockNumY()) {
            status = STATUS_DESTROYED;
            return;
        }


        directionIN = directionOUT;
        Map.Block block = map.getBlock(blockY, blockX);
        if (reverse)
            directionOUT = block.selectInDirection();
        else
            directionOUT = block.selectOutDirection();
        if (directionOUT == Map.Block.DIR_NONE) // 最後のブロックの
            directionOUT = directionIN;         // 今までの動きをそのまま継続
        if (block.hasEdgeInDir(directionIN ^ 1) == false) {
            status = STATUS_DESTROYED;
            return;
        }

        within.left  = blockX*blockSize;
        within.top   = blockY*blockSize;
        within.right  = (blockX+1)*blockSize;
        within.bottom = (blockY+1)*blockSize;
        setParams(false);
    }

    void addStability() {
        xPosition = stableXPosition;
        yPosition = stableYPosition;
        // フラツキ度の更新
        switch (stability) {
            case SINE_CURVE:
                unstableParam += unstableParamDiff;
                unstableAmount = min(marginRate, 1.0f - marginRate) * blockSize * (float) Math.sin(unstableParam) * 0.8f;
                break;
            case RANDOM_WALK:
                if (random.nextInt(5) == 0) {
                    double r = Math.random();
                    float p = unstableAmount / unstableParam;
                    if ((Math.abs(p) < 0.8 && r >= 0.5) ||
                        (Math.abs(p) >= 0.8 && r >= 0.5 + p))
                        unstableAmount += blockSize * 0.04f;
                    else
                        unstableAmount -= blockSize * 0.04f;
                 }
                break;
        }
        // 更新したフラツキを反映
        if (stability != Stability.STABLE) {
            if (directionIN == directionOUT) { // 直進
                if (dy==0) {
                    if ((dx > 0 && marginRate <= 0.5) || (dx < 0 && marginRate > 0.5))
                        yPosition = stableYPosition - unstableAmount;
                    else
                        yPosition = stableYPosition + unstableAmount;
                } else if (dx == 0) {
                    if ((dy > 0 && marginRate <= 0.5) || (dy < 0 && marginRate > 0.5))
                        xPosition = stableXPosition + unstableAmount;
                    else
                        xPosition = stableXPosition - unstableAmount;
                }
            }
            else { // 右折・左折
                float r2 = r + unstableAmount;
                xPosition = cx + r2 * (float)Math.cos(theta);
                yPosition = cy + r2 * (float)Math.sin(theta);
            }

        }
    }

    /*
    @Override
    public Type getType() {
        return Type.Bike;
    }*/

    /*
    @Override
    public boolean isHit(BaseObject object) {
        //if (object.getType() == Type.Bike) {
        //    return false;
        //}

        return (calcDistance(this, object) < radius);
    }*/

    @Override
    public void draw(Canvas canvas) {
        if (status == STATUS_DESTROYED) return;
        /*
        Paint black = new Paint();
        black.setColor(Color.BLACK);
        canvas.drawCircle(stableXPosition, stableYPosition, radius/2, black);
        */

        canvas.drawCircle(xPosition, yPosition, radius, paint);

        if (parallel == true) {
            float diff = min(marginRate, 1.0f - marginRate) * 0.5f * blockSize;
            float x2=xPosition, y2=yPosition;
            if (directionIN == directionOUT) { // 直進
                if (dy==0) {
                    if ((dx > 0 && marginRate <= 0.5) || (dx < 0 && marginRate > 0.5))
                        y2 = yPosition - diff;
                    else
                        y2 = yPosition + diff;
                } else if (dx == 0) {
                    if ((dy > 0 && marginRate <= 0.5) || (dy < 0 && marginRate > 0.5))
                        x2 = xPosition + diff;
                    else
                        x2 = xPosition - diff;
                }
            }
            else { // 右折・左折
                float r2;
                if (r/blockSize <= 0.5) {
                    r2 = r + unstableAmount - diff;
                } else {
                    r2 = r + unstableAmount + diff;
                }
                x2 = cx + r2 * (float)Math.cos(theta);
                y2 = cy + r2 * (float)Math.sin(theta);
            }

            canvas.drawCircle(x2, y2, radius, paint);
        }
    }
}
